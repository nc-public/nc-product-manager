package com.prac.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.prac.product.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Integer>{

}
