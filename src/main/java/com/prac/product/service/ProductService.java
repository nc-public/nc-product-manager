package com.prac.product.service;

import org.springframework.stereotype.Component;

import com.prac.product.vo.ProductResponseVO;

@Component
public interface ProductService {

	ProductResponseVO fetchProducts();

}
