package com.prac.product.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prac.product.entity.Product;
import com.prac.product.repository.ProductRepository;
import com.prac.product.service.ProductService;
import com.prac.product.vo.ProductResponseVO;

@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	ProductRepository productRepository;
	

	@Override
	public ProductResponseVO fetchProducts() {
		
		ProductResponseVO responseVO = new ProductResponseVO();
		List<Product> products  = productRepository.findAll();
		if(null == products || products.size() == 0) {
			responseVO.setErrorMessgae("No product Registered Currently");
			responseVO.setErrorCode("000100");
			return responseVO;
		}
		responseVO.setProductsList(products);
		responseVO.setCount(Integer.valueOf(products.size()));
		responseVO.setMessage("Successfully Fetch the Products data from Database");
		return responseVO;
	}

}
 