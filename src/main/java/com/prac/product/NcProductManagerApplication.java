package com.prac.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NcProductManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(NcProductManagerApplication.class, args);
	}

}
