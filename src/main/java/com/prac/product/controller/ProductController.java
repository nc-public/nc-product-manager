package com.prac.product.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prac.product.service.ProductService;
import com.prac.product.vo.ProductResponseVO;

@RestController
public class ProductController {
	
	@Autowired
	ProductService productService;
	
	@SuppressWarnings({ "unused", "null" })
	@GetMapping("/fetch")
	public ResponseEntity<ProductResponseVO> fetchAllProducts(){
		
		ProductResponseVO responseVo = new ProductResponseVO();
		
		HttpStatus status = HttpStatus.OK;
		
		responseVo = productService.fetchProducts();
		if(null != responseVo)
		{
			return ResponseEntity.ok(responseVo);
		}
		responseVo.setErrorMessgae("Request Unsuccessfull");
		return ResponseEntity.ok(responseVo);
	}

}
