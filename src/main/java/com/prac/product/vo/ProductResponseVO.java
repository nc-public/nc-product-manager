package com.prac.product.vo;

import java.util.List;

import com.prac.product.entity.Product;

public class ProductResponseVO extends ResponseVO {

	public List<Product> productsList;
	public int numberOfRecords;

	public List<Product> getProductsList() {
		return productsList;
	}

	public void setProductsList(List<Product> productsList) {
		this.productsList = productsList;
	}

	public int getNumberOfRecords() {
		return numberOfRecords;
	}

	public void setNumberOfRecords(int numberOfRecords) {
		this.numberOfRecords = numberOfRecords;
	}
	
	
	
	
	
}
